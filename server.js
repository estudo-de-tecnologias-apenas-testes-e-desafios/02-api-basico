const express = require('express');
const mongoose = require("mongoose");
const requireDir = require("require-dir");
const cors = require("cors");

// Init app
const app = express();
app.use(express.json());
app.use(cors());

//Conectando o DB, um ORM transforma em objeto a sintaxe
mongoose.connect("mongodb://localhost:27017/testeAPI", {useNewUrlParser: true});

requireDir("./src/models");

app.use("/algo", require("./src/routes"));

app.listen(3000);