const express = require("express");
const routes = express.Router();

const ProductController = require("./controllers/Productcontroller");

//Primeira rota
routes.get("/products", ProductController.index);
routes.get("/products/:id", ProductController.show);
routes.post("/products", ProductController.save);
routes.put("/products/:id", ProductController.update);
routes.delete("/products/:id", ProductController.delete);

module.exports = routes;